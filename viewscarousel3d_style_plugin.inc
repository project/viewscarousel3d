<?php  //$Id$


/**
 * Implementation of views_plugin_style().
 *
 */
class viewscarousel3d_style_plugin extends views_plugin_style {
  
  
  function option_definition() {
    $options = parent::option_definition();
    /**
    items  	String  	mandatory  	items selection --> a
    itemWidth 	Integer 	mandatory 	the max width for each item
    itemHeight 	Integer 	mandatory 	the max height for each item
    itemMinWidth 	Integer 	mandatory 	the minimum width for each item, the height is automaticaly calculated to keep proportions
    rotationSpeed 	Float 	mandatory 	the speed for rotation animation
    reflectionSize 	Float 	mandatory 	the reflection size a fraction from items' height
    slowOnHover 	Boolean 	optional 	if true the rotation speed slows down when an item is hovered
    slowOnOut 	Boolean 	optional 	it true the rotation speed slows down when the cursor leaves the carousel
    **/
    $options['itemWidth'] = array('default' => 110);
    $options['itemHeight'] = array('default' => 62);
    $options['itemMinWidth'] = array('default' => 50);
    $options['rotationSpeed'] = array('default' => 1.8);
    $options['reflections'] = array('default' => 0.5);
    //$options['slowOnHover'] = array('default' => TRUE);
    //$options['slowOnOut'] = array('default' => TRUE);
    
    return $options;
  }// end function option_definition;
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    $form['itemWidth'] = array(
      '#type' => 'textfield',
      '#title' => t('Item Width'),
      '#default_value' => $this->options['itemWidth'],
      '#size' => 5,
      '#description' => t('(Integer). The maximum width for each item.'),
      '#required' => TRUE
    );
    
    $form['itemHeight'] = array(
      '#type' => 'textfield',
      '#title' => t('Item Height'),
      '#default_value' => $this->options['itemHeight'],
      '#size' => 5,
      '#description' => t('(Integer). The maximum height for each item.'),
      '#required' => TRUE
    );
    
    $form['itemMinWidth'] = array(
      '#type' => 'textfield',
      '#title' => t('Item Minimum Width'),
      '#default_value' => $this->options['itemMinWidth'],
      '#size' => 5,
      '#description' => t('(Integer). The minimum width for each item, the height is automaticaly calculated to keep proportions.'),
      '#required' => TRUE
    );
    
     $form['rotationSpeed'] = array(
      '#type' => 'textfield',
      '#title' => t('Rotation Speed'),
      '#default_value' => $this->options['rotationSpeed'],
      '#size' => 5,
      '#description' => t('(Float). The speed for rotation animation.'),
      '#required' => TRUE
    );
    
    $form['reflections'] = array(
      '#type' => 'textfield',
      '#title' => t('Reflection Size'),
      '#default_value' => $this->options['reflections'],
      '#size' => 5,
      '#description' => t('(Float). The reflection size a fraction from items\' height.'),
      '#required' => TRUE
    );
  }// end function options_form;
  
}// end class viewscarousel3d_style_plugin;
