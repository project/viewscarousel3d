<?php  //$Id$

/**
 * Implementation of hook_views_plugins().
 *
 */
function viewscarousel3d_views_plugins() {
  
  return array(
    'module' => 'viewscarousel3d',
    'style'  => array(
      'viewscarousel3d' => array(
        'title' => t('3D Views Carousel'),
        'theme' => 'viewscarousel3d_view',
        'help'  => 'Display rows as rotated 3D carousel.',
        'handler' => 'viewscarousel3d_style_plugin',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal'
      )
    )
  );
}// end function viewscarousel3d_views_plugins;
